## CONNECT 4 Jeux

### Règles du jeux de CONNECT FOUR

-   Le jeux se joue à deux, chaque joueur dépose un jetons à tour de rôle.
-   Le jeux se joue sur un espace de 7 colonnes 6 rangées.
-   L'objectif du jeux est de compléter une séquence de quatre soit vertical, horizontal ou diagonal.
-   Le gagnant est le premier qui atteint l'objectif.

### Construction générale

-   L'espace du jeux est construit sur un tableau unidimensionnel de taille 42
-   La représentation peux être faite comme suit:
```
    0  1  2  3  4  5  6
    7  8  9  10 11 12 13
    14 15 16 17 18 19 20
    21 22 23 24 25 26 27
    28 29 30 31 32 33 34
    35 36 37 38 39 40 41
```

### Trouver le gagnant

1. Rangée horizontale, les indexes de départ sont (0, 7, 14, 21, 28, 35)

```javascript
let startIndex = 7
for(let i = startIndex; i < startIndex + 7; i++)
```

2. Rangée verticale, les indexes de départ sont (0, 1, 2, 3, 4, 5, 6)

```javascript
let startIndex = 1
for(let i = startIndex; i < 42; i += 7)
```

3. Rangée diagonale /, les indexes de départ sont (null, 3, 4, 5, 6, 13, 20)

```javascript
let startIndex = 3
let max = Math.min(startIndex * 7 + 1, 42)
for(let i = startIndex; i < max; i += 6)
```

4. Rangée diagonale \\, les indexes de départ sont (14, 7, 0, 1, 2, 3, null)

```javascript
let startIndex = 7
for(let i = startIndex; i < 42; i += 8)
```

5. Optimisation en regardant les rangées centrales, par exemple, si la case 31 est vide, il est impossible d'avoir une séquence de quatre dans les 4 premières rangées donc nous n'avons pas besoin de vérifier.

### AI de base
#### L'ordinateur à la base fait 4 vérifications dans l'ordre suivant
1. Vérification d'une situation gagnante immédiate.
2. Vérification pour empêcher une situation gagnante immédiate à notre adversaire au prochain tour
3. Vérification d'une situation qui entraine une victoire garantie dans deux tour; une situation qui entraine 2 colonnes gagnantes au prochain tour.
4. Vérification pour empêcher une situation de victoire garantie dans 2 tour; une situation qui entraine 2 colonnes gagnantes à notre adversaire à son prochain tour.

#### Pour améliorer la qualité de son jeux
- Si l'ordinateur joue de façon uniquement aléatoire en appliquant les 4 vérifications notées plus haut, la qualité de son jeux n'est pas très bonne.
- La méthode employée pour améliorer la qualité de son jeux est très simple, à partir de l'état actuel du jeux, l'ordinateur joue contre lui-même jusqu'à ce qu'il ait un gagnant. Il répète cette étape nombreuses fois puis prend en note le nombre de victoire dans chaque colonne de départ et joue celle qui a mené au plus grand nombre de victoire.
