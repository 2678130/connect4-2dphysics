const path = require("path");

module.exports = {
    webpack: {
        alias: {
            '#hooks': path.resolve(__dirname, "src/shared/hooks"),
            '#wrapper': path.resolve(__dirname, "src/shared/wrapper"),
            '#pages': path.resolve(__dirname, "src/pages"),
            '#components': path.resolve(__dirname, "src/pages/components"),
            '#ui' : path.resolve(__dirname, "src/shared/ui"),
            '#img' : path.resolve(__dirname, "src/img"),
            '#routes' : path.resolve(__dirname, "src/shared/config/routes.js"),
            '#themes' : path.resolve(__dirname, "src/shared/config/themes.js"),
            '#physics' : path.resolve(__dirname, "src/shared/physics"),
            
        }
    }
}