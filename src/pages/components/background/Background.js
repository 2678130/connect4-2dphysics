import React from 'react';
import styled from 'styled-components';
import {colors} from '#themes';

const BackgroundWrapper = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${colors.carbon};
    z-index: -1000;
`

const Background = () => {
    return (
        <BackgroundWrapper>

        </BackgroundWrapper>
    );
}

export default Background;
