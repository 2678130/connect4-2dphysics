import React, { useEffect } from 'react';
import styled from 'styled-components';
import { A, LangButton } from '#ui';
import { colors } from '#themes';
import { useStore } from '#hooks';
import { animated, useSpring } from 'react-spring';


const HeaderWrapper = styled(animated.header)`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    height: 8rem;
`

const HeaderLogo = styled.div`
    margin-left: 4vw;
`

const NavigationMenu = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    gap: 3vw;
    font-size: 1.1rem;
    margin-right: 6vw;
`

const Header = () => {
    const [headerBackground] = useStore(state => state.headerBackground, colors.carbon);
    const [props, set] = useSpring(() => ({ background: headerBackground, config: { duration: 500 } }));
    const [lang] = useStore(state => state.lang, 'fr');

    useEffect(() => {
        set({ background: headerBackground })
    }, [headerBackground])

    return (
        <HeaderWrapper style={props}>
            <HeaderLogo>
                123
            </HeaderLogo>
            <NavigationMenu>
                <A>à propos</A>
                <A>contacter</A>
                <LangButton />
            </NavigationMenu>
        </HeaderWrapper>
    );
}

export default Header;
