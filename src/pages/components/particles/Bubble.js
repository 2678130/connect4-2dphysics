import React, { useLayoutEffect, useRef } from 'react';
import { useGameLoop } from '#hooks';



export const Bubble = ({ particle }) => {
    const bubbleRef = useRef();

    useGameLoop(() => {
        bubbleRef.current.style.transform = `translate3D(${particle.x}px, ${particle.y}px, 0)`;
    }, 16)

    // useLayoutEffect(() => {
    //     bubbleRef.current.style.transform = `translate3D(${particle.x}px, ${particle.y}px, 0)`;
    // })

    return (
        <div ref={bubbleRef} style={{ position: 'absolute', color: 'black', width: `${particle.radius}px`, height: `${particle.radius}px`, borderRadius: '50%', border: '1px solid black' }}>
        </div>
    );
}
