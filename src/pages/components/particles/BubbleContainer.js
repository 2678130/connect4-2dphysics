import React, { useRef, useLayoutEffect, useEffect } from 'react';
import styled from 'styled-components';
import { Bubble } from './Bubble';
import { useGameLoop, useStore } from '#hooks';
import { World } from '../../../shared/physics';

const BubbleContainerWrapper = styled.div`
    background-color: transparent;
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0;
    right:0;
    top:0;
    bottom: 0;
`


const BubbleContainer = () => {
    const boundaries = useRef();
    const world = useRef(new World(25));


    // update state
    const updateState = (elapsed) => {
        world.current.updateParticlePositions(elapsed);

        let p = world.current.getParticles();
    }
    useGameLoop(updateState);

    useLayoutEffect(() => {
        const rect = boundaries.current.getBoundingClientRect();
        world.current.setBoundaries(rect.width, rect.height);
    })


    return (
        <BubbleContainerWrapper ref={boundaries}>
            {world.current.getParticles().map((particle, index) => {
                return <Bubble particle={particle} key={index} />
            })}
        </BubbleContainerWrapper>
    );
}

export default BubbleContainer;
