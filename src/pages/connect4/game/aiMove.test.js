import { getBasicMove, getAiMove, playRandomGame } from './';

const w1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    2, 2, 2, null, null, null, null
]
const wnull = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]
const w2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, 2, 2, null, null, null,
    null, null, 1, 1, null, null, null
]
const w3 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, 2, null, null, null,
    null, null, 1, 1, null, null, null
]

describe.skip('test ai', () => {
    test.skip('find winning move', () => {
        expect(getBasicMove(w1, 2)).toBe(38);
    })
    test.skip('find blocking move', () => {
        expect(getBasicMove(w1, 1)).toBe(38);
    })
    test.skip('best outcome start', () => {
        expect(getAiMove(wnull)).toBe(38);
    })
    test.skip('dont lose to squeeze', () => {
        expect(getAiMove(w3)).toBe(36)
    })
    test('to find future winner', () => {
        expect(getAiMove(w2)).toBe(36 || 39)
    })
})