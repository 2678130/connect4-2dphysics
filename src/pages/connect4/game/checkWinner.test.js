import { checkWinner } from '.';

const testArrayNull = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]
const testArrayCol1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, 2
]
const testArrayCol2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, 1, null, null, null,
    null, null, null, 1, null, null, null,
    null, null, null, 1, null, null, null,
    null, null, null, 1, null, null, null
]
const testArrayRow1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, 2, 2, 2, 2
]
const testArrayRow2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    1, 1, 1, 1, null, null, null
]
const testArrayRow3 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    1, null, 1, 1, 1, null, null
]
const testArrayDL1 = [
    null, null, null, 2, null, null, null,
    null, null, null, null, 2, null, null,
    null, null, null, null, null, 2, null,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]
const testArrayDL2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    1, null, null, null, null, null, null,
    null, 1, null, null, null, null, null,
    null, null, 1, null, null, null, null,
    null, null, null, 1, null, null, null
]
const dlL3 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    1, null, null, null, null, null, null,
    null, 1, null, null, null, null, null,
    null, null, 1, null, null, null, null,
    null, null, null, null, null, null, null
]
const testArrayDR1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, 1,
    null, null, null, null, null, 1, null,
    null, null, null, null, 1, null, null,
    null, null, null, 1, null, null, null,
    null, null, null, null, null, null, null
]
const testArrayDR2 = [
    null, null, null, null, null, null, null,
    null, null, null, 2, null, null, null,
    null, null, 2, null, null, null, null,
    null, 2, null, null, null, null, null,
    2, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]
const startsArr = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    1, 1, 2, 1, 1, 1, ,
    null, 2, null, null, null, null, null,
    2, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]
const exotic1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, 1   , null, null, null, null, null,
    1   , null, null, null, null, null, 1   ,
    null, null, null, null, null, 1   , null,
    null, null, null, null, null, null, null
]
const exotic2 = [
    null, null, null, null, null, null, null,
    null, null, null, null,    1, null, null,
    null, null, null, null, null, 1   , null,
    null, null, null, null, null, null, 1   ,
    null, null, null, null, null, null, null,
    1   , null, null, null, null, null, 1
]
describe.skip('Check winner', () => {
    test.skip('checkbasic validity', () => {

        expect(testArrayRow2[35]).toBe(1);

    })
    test.skip('check columns', () => {
        expect(checkWinner(testArrayCol2)).toStrictEqual({
            winner: 1, start: 17, type: 'c'
        });
        expect(checkWinner(testArrayCol1)).toStrictEqual({
            winner: 2, start: 20, type: 'c'
        });
    })

    test.skip('check rows', () => {
        expect(checkWinner(testArrayRow2)).toStrictEqual({
            winner: 1, start: 35, type: 'r'
        });
        expect(checkWinner(testArrayRow1)).toStrictEqual({
            winner: 2, start: 38, type: 'r'
        });
        expect(checkWinner(testArrayRow3)).toBe(false);
    })
    test.skip('check left diag', () => {
        expect(checkWinner(testArrayDL1)).toStrictEqual({
            winner: 2, start: 3, type: 'dl'
        });
        expect(checkWinner(testArrayDL2)).toStrictEqual({
            winner: 1, start: 14, type: 'dl'
        });
    })

    //13 - 10
    test.skip('check right diag', () => {
        expect(checkWinner(testArrayDR1)).toStrictEqual({
            winner: 1, start: 13, type: 'dr'
        });
        expect(checkWinner(testArrayDR2)).toStrictEqual({
            winner: 2, start: 10, type: 'dr'
        });
    })

    test.skip('check correct starts', () => {
        expect(checkWinner(startsArr)).toStrictEqual([3,4,5,6,13,20]);
    })

    test.skip('exotic diag right', () =>{
        expect(checkWinner(exotic1)).toBe(false);
    })
    test('exotic diag left', () =>{
        expect(checkWinner(exotic2)).toBe(false);
    })
})