import { findMoves } from '.';

const testArrayNull = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]
const testArrayF1 = [
    null, null, null, null, null, null, 1,
    null, null, null, null, null, null, 1,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, 1,
    null, 2, null, null, null, null, 2,
    null, 2, null, null, null, null, 1
]
const testArrayF2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, 1,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, 1,
    null, null, null, null, null, null, 2,
    null, null, null, null, null, null, 1
]
const f3 = [
    null, null, null, null, null, null, null,
    1, null, null, null, null, null, 1,
    1, null, null, null, null, null, 2,
    1, null, null, null, null, null, 1,
    1, null, null, null, null, null, 2,
    1, null, null, null, null, null, 1
]
const f4 = [
    null, null, null, null, null, null, null,
    1, 1, null, null, null, null, 1,
    1, 1, null, null, null, null, 2,
    1, 1, null, null, null, null, 1,
    1, 1, null, null, null, null, 2,
    1, 1, null, null, null, null, 1
]

describe.skip('Find moves', () => {
    test.skip('num moves', () => {
        expect(findMoves().length).toBe(7);
    })
    test.skip('empty', () => {
        expect(findMoves(testArrayNull)).toStrictEqual([35, 36, 37, 38, 39, 40, 41])
    })
    test.skip('full row', () => {
        expect(findMoves(testArrayF1)).toStrictEqual([35, 22, 37, 38, 39, 40, null])
    })
    test.skip('almost full row', () => {
        expect(findMoves(testArrayF2)).toStrictEqual([35, 36, 37, 38, 39, 40, 6])
    })
    test('almost full 2', () => {
        expect(findMoves(f3)).toStrictEqual([0, 36, 37, 38, 39, 40, 6])

    })
    test('almost full 3', () => {
        expect(findMoves(f4)).toStrictEqual([0, 1, 37, 38, 39, 40, 6])
    })
})