import {checkWinner} from './checkWinner';
import {findMoves} from './findMoves';
import {getImmediateWinningMoves} from './optimization/getImmediateWinningMoves';
import {getFutureWinningMoves} from './optimization/getFutureWinningMoves';
import {getBasicMove, getAiMove, playRandomGame} from './aiMove';


export {
    checkWinner,
    findMoves,
    getImmediateWinningMoves,
    getFutureWinningMoves,
    getBasicMove,
    getAiMove,
    playRandomGame
}