import { futureWinningMove } from './getFutureWinningMoves';

const testArrayNull = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    2, 2, null, null, null, null, null,
    1, 1, null, null, null, null, null
]

const testArrayW1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, 1, null, null, null, null,
    null, null, 1, 1, null, null, null,
    null, null, 2, 2, null, null, null
]

const testArrayW2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    1,    null, null, null, null, null, null,
    2,    2,    1,    null, null, null, null,
    2,    1,    2,    1, null, null, null,
    2,    2,    1,    2, null, null, null
]
const testArrayW3 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]

describe.skip('future winning move', () => {
    test.skip('future 1', () => {
        expect(futureWinningMove(testArrayNull)).toStrictEqual([]);
    })

    test.skip('future 2', () => {
        expect(futureWinningMove(testArrayW1)).toStrictEqual([36, 39]);
    })
    test('future 3', () => {
        expect(futureWinningMove(testArrayW2, 1)).toStrictEqual([15]);
    })
    test('future 4', () => {
        expect(futureWinningMove(testArrayW1)).toStrictEqual([36, 39]);
    })
})