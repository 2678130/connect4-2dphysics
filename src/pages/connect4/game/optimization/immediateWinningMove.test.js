import { getImmediateWinningMove } from './getImmediateWinningMoves';

const testArrayNull = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null
]

const testArrayW1 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    2, 2, 2, null, null, null, null
]

const testArrayW2 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, 1, null, null, null, null,
    null, null, 1, null, null, null, null,
    null, null, 1, 2, null, null, null
]
const testArrayW3 = [
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, null, null, null, null, null,
    null, null, 1,    null, null, null, null,
    null, null, 1,    1,    null, null, null,
    2,    null, 2,    2,    null, null, null
]



describe.skip('immediate winning move', () => {
    test.skip('turn', () => {
        expect(getImmediateWinningMove(testArrayNull)).toBe(1);
        expect(getImmediateWinningMove(testArrayW1)).toBe(2);
        
    })
    test.skip('winning 1', () =>{
        expect(getImmediateWinningMove(testArrayW1)).toStrictEqual([38]);
    })
    test.skip('winning 2', () =>{
        expect(getImmediateWinningMove(testArrayW2)).toStrictEqual([16]);
    })
    test('winning 3', () =>{
        expect(getImmediateWinningMove(testArrayW3, 2)).toStrictEqual([36]);
    })
})