import Connect4 from './Connect4';
import Board from './board/Board';
import Control from './Control';


export default Connect4;
export { 
    Board,
    Control,
};