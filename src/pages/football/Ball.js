import React, { useLayoutEffect, useRef } from 'react';
import styled from 'styled-components';
import { useGameLoop } from '#hooks'

const BallWrapper = styled.div`
    position: absolute;
    width: 40px;
    height: 40px;
    border-radius:50%;
    background-color: #fff;
`

const Ball = ({ particle }) => {
    const ballRef = useRef(null);
    const update = () => {
        if (ballRef.current) {
            ballRef.current.style.transform = `translate(${particle.x}px, ${particle.y}px)`;
        }
    }
    useGameLoop(update);
    return (
        <BallWrapper ref={ballRef}></BallWrapper>
    );
}

export default Ball;