import React, { useLayoutEffect, useRef } from 'react';
import styled, { keyframes } from 'styled-components';
import { useGesture } from 'react-use-gesture';
import { animated } from 'react-spring';
import Ball from './Ball';
import { useGameLoop } from '#hooks';
import { World, Particle, Vector2, Vector } from '#physics';

const enter = keyframes`
    from{
        opacity: 0;
    }
    to{
        opacity: 1;
    }
`

const FootballWrapper = styled.div`
    position:relative;
    width:100%;
    height: 100%;
    cursor:none;
    animation: ${enter} ease 1.5s forwards;
`

const Goal = styled.div`
    position: absolute;
    top: 0;
    left: 50%;
    transform: translate3D(-50%, 0px, 1px);
    width: 200px;
    height: 50px;
    background-color:#fff;
    transform-style: preserve-3d;
    border-radius: 0 0 20px 20px;
`

const Foot = styled(animated.div)`
    display:none;
    position: absolute;
    width: ${props => props.width}px;
    height: ${props => props.width}px;
    background-color: #fff;
    border-radius: 50%;
    transform-style: preserve-3d;
`

const Football = () => {
    console.log('render');
    const footRadius = 25;
    const ballRadius = 20;
    const containerRef = useRef(null);
    const ball = new Particle([0, 0], 2 * ballRadius, new Vector2(0, 0));
    const userParticle = new Particle([0, 0], 2 * footRadius, new Vector2(0, 0));
    const world = new World(0, 0);
    world.addParticle(ball);
    world.addUserParticle(userParticle);
    const environment = useRef(world);
    const footRef = useRef(null);
    const previousPosition = useRef(null);
    const previousTime = useRef(0);
    const previousVector = useRef(null);

    const updatePreviousPosition = () => {
        const footPosition = footRef.current.getBoundingClientRect();
        const x = footPosition.x;
        const y = footPosition.y;

        if (!previousPosition.current) {
            previousPosition.current = { x: x, y: y };
            return;
        }
        const currentTime = performance.now();
        const elapsed = currentTime - previousTime.current;
        const position = { x: x, y: y };
        let vector = new Vector2(previousVector.current.x, previousVector.current.y);
        if (elapsed > 50) {
            const x2 = previousPosition.current.x;
            const y2 = previousPosition.current.y;
            const dx = (x - x2) / elapsed;
            const dy = (y - y2) / elapsed;
            vector = Vector.scalarProduct(new Vector2(dx, dy), 10);
            previousTime.current = currentTime;
            previousPosition.current.x = x;
            previousPosition.current.y = y;
            previousVector.current = vector;
        }

        world.updateUserParticle(vector, position);
    }


    const bind = useGesture({
        onDragStart: () => {
            previousVector.current = new Vector2(0, 0);
            environment.current.userParticle.setLastCollision(null);
        },
        onMove: ({ down, xy }) => {
            footRef.current.style.display = 'block';
            footRef.current.style.filter = 'opacity(25%)'
            let x = xy[0];
            let y = xy[1];

            // Move the foot (Cursor)
            footRef.current.style.transform = `translate(${x - footRadius}px, ${y - footRadius}px)`;
            if (down) {
                footRef.current.style.filter = 'opacity(100%)'

                updatePreviousPosition();
            }
        },
        onDragEnd: () => {
            previousPosition.current = null;
            previousVector.current = null;
            environment.current.userParticle.setLastCollision(null);
        }
    })

    function updateGameLoop(elapsed) {
        environment.current.updateParticlePositions(elapsed);
        // resetCollision();
    }
    useGameLoop(updateGameLoop);

    useLayoutEffect(() => {
        const container = containerRef.current.getBoundingClientRect();
        environment.current.setBoundaries(container.width, container.height);
        const ball = environment.current.getParticles()[0];
        ball.x = container.width / 2 - ball.radius / 2;
        ball.y = container.height / 2 - ball.radius / 2;
    }, [])

    return (
        <FootballWrapper {...bind()} ref={containerRef} >
            <Goal />
            <Foot ref={footRef} width={2 * footRadius} />
            {
                environment.current.getParticles().map((p, index) => {
                    return <Ball particle={p} key={index} container={containerRef} />
                })
            }
        </FootballWrapper>
    );
}

export default Football;
