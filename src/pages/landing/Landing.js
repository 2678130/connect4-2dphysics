import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import { colors } from '#themes';
import { useScrollTo, useStore } from '#hooks';
import Scrambler from '../components/scrambler/Scrambler';
import { useSpring, animated } from 'react-spring';
import Connect4 from '../connect4/Connect4';
import { ArrowDown } from '../../shared/ui';


const LandingWrapper = styled.main`    
`
const ScrollLocation = styled.div`
    width: 100%;
    height: 100vh;
    overflow: hidden;
    background-color: ${props => props.backgroundColor};
`

const ScramblerWrapper = styled(animated.div)`
    position: fixed;
    width: 100%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`

const ArrowDownWrapper = styled.div`
    position: fixed;
    bottom: 2rem;
    left: 50%;
    transform: translate(-50%, 0);
`

const ArrowUpWrapper = styled.div`
    position: fixed;
    bottom: 2rem;
    left: 50%;
    transform: translate(-50%, 0);
`

const Landing = () => {
    const [scrollPosition, setScrollPosition] = useState(0);
    const connect4Ref = useRef(null);
    const scrollRef1 = useRef(null);
    const scrollRef2 = useRef(null);
    const [headerBackground, setHeaderBackground] = useStore(state => state.headerBackground);
    const [text, setText] = useState('Testing');
    const { scrollDown, scrollUp } = useScrollTo([scrollRef1, scrollRef2], callback);

    const [scramblerProps, setScrambler] = useSpring(() => ({ opacity: 1, config: { duration: 1000 } }))
    const [connect4Props, setConnect4Props] = useSpring(() => ({ opacity: 0, config: { duration: 1000 } }))

    function callback(position) {

        // Initial landing spot
        if (position === 0) {
            setScrollPosition(0);
            setHeaderBackground(colors.carbon);
            setScrambler({ opacity: 1 });
            setText('Testing');
            setConnect4Props({ opacity: 0, config: { duration: 200 } })
        };

        // Connect 4
        if (position === 1) {
            setScrollPosition(1);
            setScrambler({ opacity: 1 });
            setHeaderBackground(colors.chaosBlack);
            setText('Play against a Monte-Carlo tree search based ai in the game of Connect Four');
            setTimeout(() => {
                setConnect4Props({ opacity: 1, config:{duration:1000} })
                setScrambler({ opacity: 0 });
            }, 5000)
        }
    }

    return (
        <LandingWrapper>
            <ScramblerWrapper style={scramblerProps}>
                <Scrambler sentence={text} />
            </ScramblerWrapper>
            <ScrollLocation ref={scrollRef1} backgroundColor={colors.carbon}>
            </ScrollLocation>
            <ScrollLocation ref={scrollRef2} backgroundColor={colors.chaosBlack}>
                {
                    <Connect4 ref={connect4Ref} props={connect4Props} />
                }
            </ScrollLocation>
            {scrollPosition < 1 &&
                <ArrowDownWrapper>
                    <ArrowDown onClick={scrollDown} />
                </ArrowDownWrapper>
            }
        </LandingWrapper>
    );
}

export default Landing;
