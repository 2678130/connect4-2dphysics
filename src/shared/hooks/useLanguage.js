import React from 'react';
import { useStore } from '#hooks';

const useLanguage = () => {
    const [currentLanguage, setLang] = useStore(state => state.lang, 'fr');

    // 2 langs FR and EN
    const changeLanguage = () => {
        currentLanguage === 'fr' ? setLang('en') : setLang('fr');
    }

    const oppositeLanguage = () => {
        return currentLanguage === 'fr' ? 'en' : 'fr';
    }

    return {
        oppositeLanguage,
        changeLanguage,
        currentLanguage
    }
}

export default useLanguage;
