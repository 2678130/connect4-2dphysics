import {detectCollision, distance} from './detectCollision';

describe.skip('test collision detection', () => {
    test('collision true basic',() => {
        let p1 = [0,0,10];
        let p2 = [3,3,10];
        expect(detectCollision(p1, p2)).toBe(true)
    })
    test('collision false basic',() => {
        let p1 = [0,0,10];
        let p2 = [20,20,10];
        expect(detectCollision(p1, p2)).toBe(false)
    })
    test('collision true negative',() => {
        let p1 = [0,0,10];
        let p2 = [-3,-3,10];
        expect(detectCollision(p1, p2)).toBe(true)
    })
})

describe.skip('test distance measurement', () => {
    test('distance', () =>{
        let p1 = [5,4,10];
        let p2 = [2,0,10];
        expect(distance(p1, p2)).toBe(5);
    })

    test('distance2', () =>{
        let p1 = [6,0,10];
        let p2 = [1,12,10];
        expect(distance(p1, p2)).toBe(13);
    })
})