import { checkV1CollisionResolution } from './getParticleCollisionResolution';

describe('particle collision resolution', () => {
    describe('component vector resolution', () => {
        it('test1', () => {
            expect(checkV1CollisionResolution()).toBe(2);
        })
    })
})