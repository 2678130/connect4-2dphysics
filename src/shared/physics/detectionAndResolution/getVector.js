
export const getVector = (c1, c2, m = 1) => {
    const vx = c1[0] - c2[0];
    const vy = c1[2] - c2[2];
    return { vx, vy, m };
}