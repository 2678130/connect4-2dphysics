import A from './inputs/A';
import ArrowDown from './inputs/ArrowDown';
import LangButton from './inputs/LangButton';


export {
    A,
    ArrowDown,
    LangButton,
}