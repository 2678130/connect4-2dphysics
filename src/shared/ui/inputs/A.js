import styled from 'styled-components';
import {colors} from '#themes';

const A = styled.a`
    color: ${colors.eyelashViper};
    font-size: inherit;
    cursor: pointer;
`

export default A;